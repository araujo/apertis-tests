#include <stdlib.h>
#include <clutter/clutter.h>

int
main (int argc, char *argv[])
{
  ClutterActor *stage, *handle;
  ClutterAction *raction;
  GError *error;

  clutter_x11_enable_xinput();

  error = NULL;
  if (clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
      return EXIT_FAILURE;

  stage = clutter_stage_new ();
  clutter_stage_set_title (CLUTTER_STAGE (stage), "Drag Test");
  clutter_actor_set_size (stage, 800, 600);
  g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

  handle = clutter_actor_new ();
  clutter_actor_set_background_color (handle, CLUTTER_COLOR_SkyBlue);
  clutter_actor_set_size (handle, 300, 300);
  clutter_actor_set_position (handle, (800 - 300) / 2, (600 - 300) / 2);
  clutter_actor_set_reactive (handle, TRUE);
  clutter_actor_add_child (stage, handle);
  clutter_actor_set_pivot_point(handle, 0.5, 0.5);

  raction = clutter_zoom_action_new ();
  clutter_actor_add_action (handle, raction);

  clutter_actor_add_effect_with_name (handle, "disable", clutter_desaturate_effect_new (0.0));

  clutter_actor_show (stage);

  clutter_main ();

  return EXIT_SUCCESS;
}
