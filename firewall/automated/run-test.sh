#!/bin/bash
# vim: set sts=4 sw=4 et tw=0 :
#

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - &>/dev/null)

. "${TESTDIR}/config.sh"

#########
# Setup #
#########
setup_success

###########
# Execute #
###########

test_iptables_module() {
    local ret=0

    lsmod | grep -q '^iptable_filter' || return 1

    return $ret
}
test_iptables_service() {
    local ret=0 LoadState UnitFileState ActiveState

    # Check the following values:
    #   LoadState=loaded
    #   UnitFileState=enabled
    #   ActiveState=active

    eval $(systemctl show iptables.service \
        | grep -E '^LoadState=|^UnitFileState=|^ActiveState=')
    test "$LoadState" = "loaded" || return 1
    test "$UnitFileState" = "enabled" || return 1
    test "$ActiveState" = "active" || return 1
    return $ret
}

test_iptables_list() {
    local ret=0

    read -d '' expected_rules <<EOF
-P INPUT ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -i gpic0 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
-A INPUT -p udp -m udp --dport 1900 -j ACCEPT
-A INPUT -d 224.0.0.251/32 -p udp -m udp --dport 5353 -j ACCEPT
-A INPUT -i tether -p udp -m udp --dport 67 -j ACCEPT
-A INPUT -i tether -p udp -m udp --dport 53 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
EOF

    read -d '' actual_rules < <(sudo iptables --list-rules INPUT)

    diff -u <(echo "$expected_rules") <(echo "$actual_rules")

    test "$expected_rules" = "$actual_rules" || return 1
    return $ret
}

trap "test_failure" ERR

src_test_pass <<-EOF
test_iptables_module
test_iptables_service
test_iptables_list
EOF

test_success
