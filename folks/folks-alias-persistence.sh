#!/bin/bash

echo "# running test: $0"

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - &>/dev/null)

mkdir -p ~/.local/share/folks

# Clean up to make sure we have a good test.
$cur_dir/remove-tp-accounts.sh

# Start test.
# Create test111 account.
$cur_dir/create-account.sh 1

TMPFILE=`tempfile`
folks-inspect individuals > ${TMPFILE} 2>&1
test333_id=`grep -B 30 "test333" ${TMPFILE} | grep "Individual" | awk -F"'" '{ print $2 }'`
rm ${TMPFILE}

if test "$test333_id" == ""; then
  echo "folks-alias-persistence: FAILED"
  exit 1
fi

if ! folks-inspect set alias $test333_id crazyaliasnobodywouldeverset; then
  echo "folks-alias-persistence: FAILED"
  exit 1
fi

output=`folks-inspect individuals 2>&1 | grep crazyaliasnobodywouldeverset`

# Clean up after the test.
$cur_dir/remove-tp-accounts.sh

if test "z$output" != "z"; then
  echo "folks-alias-persistence: PASSED"
  exit 0
else
  echo "folks-alias-persistence: FAILED"
  exit 1
fi

