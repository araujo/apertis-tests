#!/bin/bash

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - &>/dev/null)

if test "$1" = "1"; then
id=`mc-tool list | grep test111`
mc-tool enable $id
mc-tool auto-connect $id on

elif test "$1" = "2"; then
id=`mc-tool list | grep test222`
mc-tool enable $id
mc-tool auto-connect $id on

elif test "$1" = "3"; then
id=`mc-tool list | grep test333`
mc-tool enable $id
mc-tool auto-connect $id on

elif test "$1" = "4"; then
id=`mc-tool list | grep test444`
mc-tool enable $id
mc-tool auto-connect $id on
fi
