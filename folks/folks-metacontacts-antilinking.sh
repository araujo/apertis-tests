#!/bin/bash

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - &>/dev/null)

# Clean up to make sure we have a good test.
$cur_dir/remove-tp-accounts.sh
# Remove all contacts from eds.
syncevolution --delete-items backend=evolution-contacts @foo bar '*'

# Start test.
# Create test111 and test222 account.
$cur_dir/create-account.sh 1
$cur_dir/create-account.sh 2

# Get the gabble.echo individual, so we can unlink it.
# FIXME: This depends on folks-inspect individuals output for an individual being 35 lines long
# Possibly look into using some perl script to make this test more robust
individual_id=`folks-inspect individuals 2>&1 | grep -B 35 "gabble.echo" | grep "Individual" | awk -F"'" '{ print $2 }'`

# Unlink gabble.echo so antilink will be created.
folks-inspect linking unlink-individual $individual_id

# Disable then enable both tp accounts.
$cur_dir/disable-account.sh 1
$cur_dir/disable-account.sh 2

$cur_dir/enable-account.sh 1
$cur_dir/enable-account.sh 2

# Verify antilink prevented gabble.echo personas from linking
output=`folks-inspect individuals 2>&1 | grep "jabber.*gabble.echo"`
outputlines=`echo "$output" | wc -l`

# Clean up after the test.
$cur_dir/remove-tp-accounts.sh

# Check test result
if test "$outputlines" = "2"; then
  echo "folks-metacontacts-antilinking: PASSED"
  exit 0
else
  echo "folks-metacontacts-antilinking: FAILED"
  echo "output was $output"
  exit 1
fi

