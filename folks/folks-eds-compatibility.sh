#!/bin/bash

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - &>/dev/null)

# Remove all contacts from eds.
syncevolution --delete-items backend=evolution-contacts @foo bar '*'

# Add one Local 1 contact into eds.
syncevolution --import $cur_dir/local1.vcf backend=evolution-contacts @foo bar

# Ensure "Local 1" is found in folks.
output=`folks-inspect individuals | grep "Local 1"`

# Clean up the eds addressbook.
syncevolution --delete-items backend=evolution-contacts @foo bar '*'

if test "z$output" != "z"; then
  echo "EDS Compatibility: PASSED"
  exit 0
else
  echo "EDS Compatibility: FAILED"
  exit 1
fi

