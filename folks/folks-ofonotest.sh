#!/bin/bash

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

# Debuggability
set -x

cur_dir=$(cd $(dirname $0); pwd; cd - &>/dev/null)

mkdir -p ~/.local/share/folks

export FOLKS_BACKEND_STORE_KEY_FILE_PATH="$cur_dir/ofonobackend.ini"
folks-inspect personas

