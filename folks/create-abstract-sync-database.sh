#!/bin/bash

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

# Debuggability
set -x

database=`uuidgen`

# Create abstract database
if syncevolution --create-database type=evolution-contacts database=$database; then
    # Now remove it
    if syncevolution --remove-database type=evolution-contacts database=$database; then
        echo "create-abstract-sync-database: PASSED"
        exit 0
    else
        echo "create-abstract-sync-database: FAILED"
        exit 1
    fi
else
    echo "create-abstract-sync-database: FAILED"
    exit 2
fi
