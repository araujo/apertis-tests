#!/bin/bash

cur_dir=$(cd $(dirname $0); pwd; cd - &>/dev/null)
# Fix this hack by moving these tests to /usr/share/chaiwala-tests
common_dir=${cur_dir/lib/share}
common_dir=${common_dir/contacts/common}
. "${common_dir}/common.sh"
ensure_dbus_session

mkdir -p ~/.local/share/folks

export FOLKS_BACKEND_STORE_KEY_FILE_PATH="$cur_dir/ofonobackend.ini"
folks-inspect personas

