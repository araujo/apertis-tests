#!/bin/bash

cur_dir=$(cd $(dirname $0); pwd; cd - &>/dev/null)
# Fix this hack by moving these tests to /usr/share/chaiwala-tests
common_dir=${cur_dir/lib/share}
common_dir=${common_dir/contacts/common}
. "${common_dir}/common.sh"
ensure_dbus_session

mac_addr=$1

if test "z$mac_addr" == "z"; then
  echo "usage synce-addressbook.sh phone_mac_address"
else

# create target-config
syncevolution --configure \
                syncURL= \
                addressbook/backend=pbap \
                addressbook/database=obex-bt://$mac_addr \
                target-config@pbap addressbook

# print items
#syncevolution --print-items \
#                target-config@pbap addressbook

# configure synchronization
syncevolution --configure \
                --template SyncEvolution_Client \
		--keyring=no \
                syncURL=local://@pbap \
                pbap

# run synchronization
syncevolution --sync refresh-from-client pbap addressbook

folks-inspect personas

fi

