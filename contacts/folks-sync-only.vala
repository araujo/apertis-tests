/*
 * Copyright (C) 2013, 2015 Collabora Ltd.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Rodrigo Moya <rodrigo.moya@collabora.co.uk>
 *          Philip Withnall <philip.withnall@collabora.co.uk>
 *
 */

using Folks;
using Gee;

public class TestSyncOnly : Object
{
  private GLib.MainLoop _main_loop;
  private int _result = -1;
  private IndividualAggregator _aggregator;
  private BackendStore _backend_store = null;
  private PersonaStore _synced_store = null;
  private bool _eds_backend_ready = false;
  private Debug _debug = null;

  public TestSyncOnly ()
    {
      this._main_loop = new GLib.MainLoop (null, false);
    }

  private void _check_sync_store ()
    {
      GLib.print ("Checking store %s is being sync'ed\n", this._synced_store.display_name);

      /* Check we have a synced store in EDS backend */
      GLib.Idle.add (() =>
		{
          var eds_backend = this._backend_store.dup_backend_by_name ("eds");
          if (eds_backend != null)
            {
              foreach (var store in eds_backend.persona_stores.values)
                {
                  if (store.id == this._synced_store.display_name)
                    {
                      GLib.print ("Synced store %s created\n", this._synced_store.display_name);

                      this._result = 0;
                      this._main_loop.quit ();

                      return false;
                    }
                }
            }

          return true;
		});
    }

  private void _store_prepared (Object _pstore, ParamSpec ps)
    {
      this._check_sync_store ();
    }

  private void _backend_available (Backend backend)
    {
      GLib.print ("Backend %s available\n", backend.name);

      if (backend.name == "eds")
        {
          this._eds_backend_ready = true;
        }

      if (!this._eds_backend_ready)
        {
          return;
        }

      var eds_backend = this._backend_store.dup_backend_by_name ("eds");
      if (eds_backend != null && this._synced_store == null)
        {
          /* EDS backend available, so try to get a store to synchronize */
          foreach (var b in this._backend_store.enabled_backends.values)
            {
              foreach (var s in b.persona_stores.values)
                {
                  GLib.print ("Found store %s in backend %s with sync_mode = %d\n", s.display_name, b.name, s.sync_mode);

                  if (b.name != "eds" && this._synced_store == null)
                    {
                      if (s.sync_mode == Folks.PersonaStoreSyncMode.SYNCHRONIZE_ONLY)
                        {
                          GLib.print ("Found store %s to sync, %s\n", s.display_name,
                                      s.is_quiescent ? "already prepared" : "not ready yet");

                          this._synced_store = s;
                          if (this._synced_store.is_quiescent)
                            {
                              this._check_sync_store ();
                            }
                          else
                            {
                              this._synced_store.notify["is-quiescent"].connect (this._store_prepared);
                            }
                        }
                    }
                }
            }
        }
    }

  public int run ()
    {
      /* Load backends */
      this._debug = Debug.dup ();
	  this._backend_store = BackendStore.dup ();
      this._backend_store.backend_available.connect (this._backend_available);
	  this._backend_store.prepare.begin ((o, r) =>
		{
          this._backend_store.prepare.end (r);
		});

      this._aggregator = IndividualAggregator.dup ();
	  this._aggregator.prepare.begin ((o, r) =>
		{
		  try
			{
			  this._aggregator.prepare.end (r);
            }
          catch (GLib.Error e)
		    {
			  GLib.printerr ("Failed to prepare aggregator: %s\n", e.message);
			  this._debug.emit_print_status ();
			  this._main_loop.quit ();
			}
		});

      /* Add a timeout to fail if something goes wrong */
      GLib.Timeout.add_seconds (60, () =>
		{
          GLib.printerr ("Timeout reached, test failed\n");
          this._debug.emit_print_status ();
          this._main_loop.quit ();

          return false;
		});

      this._main_loop.run ();

      return this._result;
    }
}

public int main (string[] args)
{
  int result;
  var test = new TestSyncOnly ();

  result = test.run ();
  if (result == 0)
    GLib.print ("folks-sync-only: PASSED\n");
  else
    GLib.print ("folks-sync-only: FAILED\n");

  return result;
}