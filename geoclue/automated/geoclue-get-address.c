/* 
 * Taken from https://developer.gnome.org/geoclue/unstable/simple-master-example.html 
 * and heavily modified.
 *
 * Copyright 2008 by Garmin Ltd. or its subsidiaries
 * Copyright 2013 Collabora Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <geoclue/geoclue-master.h>
#include <glib/gstdio.h>

static void
print_addr_field (gchar *key,
		  gchar *value,
		  gpointer user_data)
{
	g_printf ("%s:\t%s\n", key, value);
}

int main()
{
	GeoclueMaster *master;
	GeoclueMasterClient *client;
	GeoclueAddress *addr;
	GHashTable *addr_details;
	gboolean got_address = FALSE;
	GError *error = NULL;
	
	/* Create a MasterClient using Master */
	master = geoclue_master_get_default ();
	client = geoclue_master_create_client (master, NULL, &error);
	g_object_unref (master);
	
	if (!client) {
		g_printerr ("Error creating GeoclueMasterClient: %s\n", error->message);
		g_error_free (error);
		return 1;
	}
	
	/* Set our requirements: We want at least city level accuracy, require signals, 
	   and allow the use of network (but not e.g. GPS) */
	if (!geoclue_master_client_set_requirements (client,
	                                             GEOCLUE_ACCURACY_LEVEL_LOCALITY,
	                                             0, TRUE,
	                                             GEOCLUE_RESOURCE_NETWORK,
	                                             &error)){
		g_printerr ("set_requirements failed: %s", error->message);
		g_error_free (error);
		g_object_unref (client);
		return 1;
		
	}
	
	/* Get an Address object */
	addr = geoclue_master_client_create_address (client, NULL);
	if (!addr) {
		g_printerr ("Failed to get a address object");
		g_object_unref (client);
		return 1;
	}
	
	/* call get_position. We do not know which provider actually provides 
	   the answer (although we could find out using MasterClient API) */
	got_address = geoclue_address_get_address (addr,
						   NULL, /* timestamp */
						   &addr_details,
						   NULL, /* accuracy */
						   &error);
	if (!got_address) {
		g_printerr ("Error in geoclue_position_get_position: %s.\n", error->message);
		g_clear_error (&error);
		goto err;
	}

	if (g_hash_table_size (addr_details) == 0) {
		g_printerr ("Error: Geoclue reported no addresses.\n");
		goto err;
	}

	/* For details, see GEOCLUE_ADDRESS_KEY_* inside:
	 * http://cgit.freedesktop.org/geoclue/tree/geoclue/geoclue-types.h */
	g_hash_table_foreach (addr_details,
			      (GHFunc) print_addr_field,
			      NULL);
	return 0;
err:
	g_object_unref (addr);
	g_object_unref (client);
	return 1;
}
