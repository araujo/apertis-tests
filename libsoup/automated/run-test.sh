#!/bin/bash
# vim: set sts=4 sw=4 et :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - &>/dev/null)
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" ERR

setup_success

###########
# Execute #
###########
trap "test_failure" ERR

# Not all the executables are automated tests
# The 'tr' below assumes there's no spaces in the path
# The 'sed' prepends tests which are expected to be skipped with '#';
# check_file_exists_tee must be called first
echo -e "${LIBEXECDIR}/libsoup2.4/installed-tests/libsoup/"{*-test,header-parsing,date,uri-parsing} | \
    tr ' ' '\n' | \
    check_file_exists_tee | \
    sed -e '/\/\(proxy-test\|xmlrpc-test\)$/ s/^/# /' | \
    src_test_pass

test_success
