#!/bin/bash
# vim: set sts=4 sw=4 et :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - &>/dev/null)
TEST_ROOT_DIR=${TESTDIR}/../
SHAREDDIR=${TEST_ROOT_DIR}/../common

. "${SHAREDDIR}/common.sh"
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" ERR

# Copy test data to work directory
src_copy_contents "${INSTALLDIR}" "${WORKDIR}"

setup_success

###########
# Execute #
###########
trap "test_failure" ERR

cd "${WORKDIR}/demos"
# We need to skip the libtool wrapper and run the binaries directly so that the
# libraries on the installed system are used instead of the ones we just built
.libs/url-marker

test_success
