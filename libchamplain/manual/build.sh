#!/bin/bash
# vim: set sts=4 sw=4 et :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - &>/dev/null)
TEST_ROOT_DIR=${TESTDIR}/../
SHAREDDIR=${TEST_ROOT_DIR}/../common

. "${SHAREDDIR}/common.sh"
. "${TESTDIR}/config.sh"
. "${SHAREDDIR}/autotools-setup.sh"

trap "build_failure" EXIT

# Cleanup build and install directories
rm -rf "${BUILDDIR}" "${INSTALLDIR}"
mkdir -p "${BUILDDIR}" "${INSTALLDIR}"

${APT_GET} install curl

autotools_src_prepare

# Install test binaries and data
src_copy "${BUILDDIR}/demos" "${INSTALLDIR}"

build_success
