#!/bin/bash
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - &>/dev/null)
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" ERR

ensure_dbus_session
setup_success

###########
# Execute #
###########
say "Results will be in /tmp/dbus-match-rule-list-*.txt"
test_match_rules_system() {
    sudo "${TESTDIR}/match-rules.py" --system  &> /tmp/dbus-match-rule-list-system.txt
}

test_match_rules_session() {
    "${TESTDIR}/match-rules.py" --session &> /tmp/dbus-match-rule-list-session.txt
}

trap "cat /tmp/dbus-match-rule-list-*.txt ; test_failure" ERR

src_test_pass <<-EOF
test_match_rules_system
test_match_rules_session
EOF

test_success
