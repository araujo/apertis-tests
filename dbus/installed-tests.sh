#!/bin/sh

set -e

timeout="busybox timeout -t 300"
ret=0
i=0
tmpdir="$(mktemp -d)"

for dir in /usr/lib/dbus-1.0/debug-build/libexec/installed-tests/dbus /usr/lib/dbus-1.0/installed-tests/dbus
do
    DBUS_TEST_DATA="${dir}/data"
    export DBUS_TEST_DATA
    DBUS_TEST_HOMEDIR="${tmpdir}/home"
    if ! test -d "$DBUS_TEST_HOMEDIR"
    then
        mkdir "$DBUS_TEST_HOMEDIR"
    fi
    export DBUS_TEST_HOMEDIR

    case "$dir" in
        (*debug-build*)
            tag="debug-build."
            ;;
        (*)
            tag=""
            ;;
    esac

    for t in "$dir"/test-*
    do
        i=$(( $i + 1 ))
        echo "# $i - $t ..."
        echo "x" > "$tmpdir/result"
        ( set +e; $timeout $t; echo "$?" > "$tmpdir/result" ) 2>&1 | sed 's/^/# /'
        # guard against a partial line
        echo '#'
        e="$(cat "$tmpdir/result")"
        case "$e" in
            (0)
                echo "TEST_RESULT:pass:${tag}$(basename "$t"):"
                ;;
            (77)
                echo "TEST_RESULT:skip:${tag}$(basename "$t"):"
                ;;
            (*)
                echo "TEST_RESULT:fail:${tag}$(basename "$t"):exit $e"
                ret=1
                ;;
        esac
    done
done

rm -r "$tmpdir"

if [ "$i" = 0 ]
then
    echo "# Did not execute any tests?!"
    exit 2
fi

exit $ret
