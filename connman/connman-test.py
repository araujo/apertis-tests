#!/usr/bin/python

import sys
import unittest
import gobject
import dbus
import dbus.service
import dbus.mainloop.glib
import time
import re


class TestProperties(unittest.TestCase):
    def setUp(self):
        self.bus = dbus.SystemBus()
        self.manager = dbus.Interface(self.bus.get_object("net.connman", "/"),
                                      "net.connman.Manager")

    def test_manager(self):
        props = self.manager.GetProperties()

        self.assertTrue('State' in props)
        self.assertTrue('OfflineMode' in props)

    def test_offlinemode(self):
        self.manager.SetProperty("OfflineMode", True)
        props = self.manager.GetProperties()
        self.assertTrue(props["OfflineMode"])

        self.manager.SetProperty("OfflineMode", False)
        props = self.manager.GetProperties()
        self.assertFalse(props["OfflineMode"])

    def technology_enable_test(self, path):
        tech = dbus.Interface(self.bus.get_object("net.connman", path),
                              "net.connman.Technology")

        props = tech.GetProperties()
        if not props["Powered"]:
            tech.SetProperty("Powered", True)
            time.sleep(1)

        tech.SetProperty("Powered", False)
        props = tech.GetProperties()
        self.assertFalse(props["Powered"])

        tech.SetProperty("Powered", True)
        time.sleep(1)
        props = tech.GetProperties()
        self.assertTrue(props["Powered"])

    def test_technologies(self):
        technologies = self.manager.GetTechnologies()

        if len(technologies) == 0:
            print('skip technologies tests')
            return

        for path, props in technologies:
            self.assertTrue('Connected' in props)
            self.assertTrue('Powered' in props)
            self.assertTrue('Name' in props)
            self.assertTrue('Type' in props)
            self.assertTrue('Tethering' in props)
            if props['Tethering']:
                # Not available all the times
                self.assertTrue('TetheringIdentifier' in props)
                self.assertTrue('TetheringPassphrase' in props)

            self.technology_enable_test(path)

    def test_ethernet(self):
        services = self.manager.GetServices()
        for s in services:
            result = re.search('ethernet_.*', s[0])
            if result is None:
                continue
            path = result.group(0)
            if path is not None:
                break

        self.assertTrue(path)

        prefixed_path = "/net/connman/service/" + path
        eth = dbus.Interface(self.bus.get_object("net.connman", prefixed_path),
                             "net.connman.Service")

        try:
            eth.Disconnect()
        except:
            pass

        time.sleep(3)

        eth.Connect()

        props = eth.GetProperties()
        self.assertTrue('State' in props)
        self.assertTrue('Name' in props)
        self.assertTrue('Type' in props)
        self.assertTrue('Favorite' in props)
        self.assertTrue('Immutable' in props)
        self.assertTrue('AutoConnect' in props)
        self.assertTrue('Nameservers' in props)
        self.assertTrue('Nameservers.Configuration' in props)
        self.assertTrue('Timeservers' in props)
        self.assertTrue('Timeservers.Configuration' in props)
        self.assertTrue('Domains' in props)
        self.assertTrue('Domains.Configuration' in props)
        self.assertTrue('IPv4' in props)
        self.assertTrue('IPv4.Configuration' in props)
        self.assertTrue('IPv6' in props)
        self.assertTrue('IPv6.Configuration' in props)
        self.assertTrue('Proxy' in props)
        self.assertTrue('Proxy.Configuration' in props)
        self.assertTrue('Provider' in props)
        self.assertTrue('Ethernet' in props)

        eth.SetProperty('Timeservers.Configuration',
                        ['0.fedora.pool.ntp.org',
                         '1.fedora.pool.ntp.org',
                         '2.fedora.pool.ntp.org'])

        eth.SetProperty('Nameservers.Configuration',
                        ['4.4.4.4', '8.8.8.8'])

        eth.SetProperty('AutoConnect', False)
        props = eth.GetProperties()
        self.assertFalse(props['AutoConnect'])
        eth.SetProperty('AutoConnect', True)
        props = eth.GetProperties()
        self.assertTrue(props['AutoConnect'])

    def test_clock(self):
        clock = dbus.Interface(self.bus.get_object("net.connman", "/"),
                               "net.connman.Clock")
        props = clock.GetProperties()

        self.assertTrue('TimeUpdates' in props)
        self.assertTrue('Timezone' in props)
        self.assertTrue('Timeservers' in props)
        self.assertTrue('TimezoneUpdates' in props)
        self.assertTrue('Time' in props)

        clock.SetProperty('TimeUpdates', 'manual')
        clock.SetProperty('Time', props['Time'])
        clock.SetProperty('Timeservers',
                          ['0.fedora.pool.ntp.org',
                           '1.fedora.pool.ntp.org',
                           '2.fedora.pool.ntp.org'])
        clock.SetProperty('TimeUpdates', 'auto')
        clock.SetProperty('TimezoneUpdates', 'manual')
        clock.SetProperty('Timezone', 'America/Vancouver')
        clock.SetProperty('TimezoneUpdates', 'auto')

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestProperties)
    unittest.TextTestRunner(verbosity=2).run(suite)
