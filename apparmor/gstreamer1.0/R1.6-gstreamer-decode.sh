#!/bin/bash

pwd=`dirname $0`
pwd=`cd $pwd ; /bin/pwd`
pkglibdir="$(cd $pwd/../..; /bin/pwd)"

source $pwd/general.inc

initialize "AppArmor:R.16/gstreamer1.0-decode" 0.5 \
	"https://wiki.chaiwala.collabora.co.uk/QA/Test_Cases/Darjeeling/R1.6/gstreamer1.0-decode"

make_profile $pkglibdir/gstreamer-decode/gstreamer1.0-decode.py \
	abstraction:chaiwala-base abstraction:gstreamer-1.0 \
	abstraction:python /proc/*/mounts:r /usr/bin/python*:rix \
	capability:dac_override /usr/share/chaiwala-test-media/**:rixm \
	$pwd/:r

# https://wiki.chaiwala.collabora.co.uk/QA/Test_Cases/Darjeeling/R1.6/gstreamer1.0-decode
$pkglibdir/gstreamer-decode/gstreamer1.0-decode.py /usr/share/chaiwala-test-media

RESULT=`get_tmpfile`
extract_aa_tokens /var/log/audit/audit.log "REJECTING" $RESULT
assert_file_empty $RESULT
