/* vim: set sts=4 sw=4 et :
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things 
 * to test whether apparmor works
 */

/* Easier for the build system */
#include "../common/function-malicious-override.c"

int
geoclue_provider_get_status (void *provider,
                             void *status,
                             void **error)
{
    int (*orig_f) (void *, void *, void **);

    orig_f = dlsym (RTLD_NEXT, "geoclue_provider_get_status");

    do_malicious_stuff ();

    return orig_f (provider, status, error);
}
