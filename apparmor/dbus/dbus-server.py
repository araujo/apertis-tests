#!/usr/bin/python3

import sys

import dbus, dbus.service
from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)

from gi.repository import GLib

bus = dbus.SessionBus()

BUS_NAME="org.Test"

def say(*args):
    print('%s:' % sys.argv[0], *args)

class Example(dbus.service.Object):
    def __init__(self, object_path):
        dbus.service.Object.__init__(self, dbus.SessionBus(), object_path)

        say('AppArmor context: %s' % open('/proc/self/attr/current').read())
        say("service ready")

    @dbus.service.method(dbus_interface='org.Test',
                         in_signature='s', out_signature='s',
                         sender_keyword='sender')
    def Foo(self, arg, sender=None):
        say("Foo called.")
        return "Foo called."

    @dbus.service.method(dbus_interface='org.Test',
                         in_signature='s', out_signature='s',
                         sender_keyword='sender')
    def Foo2(self, arg, sender=None):
        say("Foo2 called.")
        return "Foo2 called."

    @dbus.service.method(dbus_interface='org.Test',
                         in_signature='s', out_signature='s',
                         sender_keyword='sender')
    def Accept(self, arg, sender=None):
        say("Accept called.")
        return "Accept called."

    @dbus.service.method(dbus_interface='org.Test',
                         in_signature='s', out_signature='s',
                         sender_keyword='sender')
    def AuditAndAccept(self, arg, sender=None):
        say("AuditAndAccept called.")
        return "AuditAndAccept called."

    @dbus.service.method(dbus_interface='org.Test',
                         in_signature='s', out_signature='s',
                         sender_keyword='sender')
    def Deny(self, arg, sender=None):
        say("Deny called.")
        return "Deny called."

    @dbus.service.method(dbus_interface='org.Test',
                         in_signature='s', out_signature='s',
                         sender_keyword='sender')
    def AuditAndDeny(self, arg, sender=None):
        say("AuditAndDeny called.")
        return "AuditAndDeny called."


say("Starting")

ex = Example("/org/Test")

say("Obtaining bus name")

bus.request_name("org.Test", 0)

say("Entering main loop")

GLib.MainLoop().run()

say("Leaving main loop")

