#!/bin/bash
# vim: set sts=4 sw=4 et tw=0 :

set -e

TEST_DIR="$(cd "$(dirname "$0")" && pwd)"

if [[ ${#@} -eq 0 ]]; then
    :
else
    echo "Usage: $0"
    exit 1
fi

${TEST_DIR}/../../folks/folks-metacontacts-antilinking.sh
