#!/bin/bash
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - &>/dev/null)
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" ERR

# oauth,oauth-sync tests need dbus, and the others try to spawn it
ensure_dbus_session

setup_success

###########
# Execute #
###########
trap "test_failure" ERR

# The 'tr' below assumes there's no spaces in the path
echo -e "${LIBEXECDIR}/librest/"{custom-*,flickr,lastfm,oauth*,proxy*,threaded} | \
    tr ' ' '\n' | check_file_exists_tee | src_test_pass

# These are expected to fail (as per upstream)
echo -e "${LIBEXECDIR}/librest/"xml | check_file_exists_tee | src_test_fail

test_success
