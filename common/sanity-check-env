#!/bin/sh

# sanity-check-env [system|user]
#
# Verify that the environment variables set for systemd services are present.

# Copyright © 2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

failed=0

fail () {
    failed=1
    tag="$1"
    shift
    echo "TEST_RESULT:fail:$tag:$*" >&2
}

echo "# Environment:"
env | sort | sed 's/^/# /g'

case ":$PATH:" in
    (*:/usr/bin:*)
        ;;
    (*)
        fail PATH "does not contain /usr/bin"
        failed=1
        ;;
esac

case "$LANG" in
    (*.UTF-8)
        ;;
    (*)
        fail LANG "not a UTF-8 locale"
        failed=1
        ;;
esac

# below this point is only valid for user services
if [ "$1" = system ]; then
    exit "$failed"
fi

[ "x$USER" = "x$(id -n -u)" ] || fail USER "should be $(id -n -u)"
[ "x$LOGNAME" = "x$(id -n -u)" ] || fail LOGNAME "should be $(id -n -u)"

case "$SHELL" in
    (/bin/bash|/bin/sh|/bin/dash)
        ;;
    (*)
        fail SHELL "neither sh, bash nor dash"
        ;;
esac

[ "x$XDG_RUNTIME_DIR" = "x/run/user/$(id -u)" ] || fail XRD "unexpected"

[ -d "$XDG_RUNTIME_DIR" ] || fail XRD "not a directory"
[ -r "$XDG_RUNTIME_DIR" ] || fail XRD "not readable"
[ -w "$XDG_RUNTIME_DIR" ] || fail XRD "not writable"
[ -x "$XDG_RUNTIME_DIR" ] || fail XRD "not traversable"

echo -n '# '
ls -l -d "$XDG_RUNTIME_DIR"
case "$(ls -l -d "$XDG_RUNTIME_DIR")" in
    (drwx------*)
        ;;
    (*)
        fail XRD "$XDG_RUNTIME_DIR has unexpected permissions"
        ;;
esac

[ "x$HOME" = "x/home/$(id -n -u)" ] || fail HOME "unexpected"

[ -d "$HOME" ] || fail HOME "not a directory"
[ -r "$HOME" ] || fail HOME "not readable"
[ -w "$HOME" ] || fail HOME "not writable"
[ -x "$HOME" ] || fail HOME "not traversable"

echo -n '# '
ls -l -d "$HOME"
case "$(ls -l -d "$HOME")" in
    (drwx??????*)
        ;;
    (*)
        fail HOME "unexpected permissions"
        ;;
esac

kill -0 "$MANAGERPID" || fail MANAGERPID "unset or cannot be signalled"

if [ "${DBUS_SESSION_BUS_ADDRESS+set}" = set ]; then
    case "$DBUS_SESSION_BUS_ADDRESS" in
        (unix:path=$XDG_RUNTIME_DIR/bus)
            ;;
        (*)
            fail DBUS_SESSION_BUS_ADDRESS "unexpected"
            ;;
    esac
else
    echo '# DBUS_SESSION_BUS_ADDRESS is unset, a default will be used'
fi

if [ -S "$XDG_RUNTIME_DIR/wayland-0" ]; then
    echo "# XDG_RUNTIME_DIR/wayland-0 is a socket, skipping X11 checks"
elif [ "${DISPLAY+set}" = set ]; then
    case "$DISPLAY" in
        # 10 DISPLAYs should be enough for anyone :-)
        (:[0-9])
            ;;
        (*)
            fail DISPLAY "unexpected"
            ;;
    esac
else
    # This is X11-specific; when we've gone to Wayland, this should be removed
    fail DISPLAY 'is unset'
fi

exit "$failed"

# vim:set sw=4 sts=4 et:
