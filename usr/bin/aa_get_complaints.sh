#!/bin/bash
# vim: set sts=4 sw=4 et tw=80 :
# 
# This script needs `aa_log_extract_tokens.pl` to be in PATH
#

set -e

#############
# Functions #
#############
sayn() {
    echo -n -e "\033[01;32m>>>\033[00m $@"
}

say() {
    # Speak in green
    sayn "$@"
    echo
}

########
# Work #
########
AA_DUMP="aa-dump_$(date +%Y%m%d-%H%M%S)"
AA_TMPDIR="$(mktemp -d /tmp/apparmor-complaints.XXXXXX)"
TMPDIR="${AA_TMPDIR}/${AA_DUMP}"
mkdir "${TMPDIR}"

sayn "Check for apparmor complaints? [y/N] "
read i
[[ $i =~ (Y|y) ]] || exit

say "Installing dependencies ..."
# Need this for the AppArmor perl module
sudo apt-get -y --force-yes install apparmor-utils

say "Checking for apparmor complaints ..."
sudo cat /var/log/audit/audit.log | aa_log_extract_tokens.pl \
    PERMITTING REJECTING > "${TMPDIR}/complaint_tokens.log"

if ! [[ -s "${TMPDIR}/complaint_tokens.log" ]]; then
    say "No complaints found!"
    exit 0
fi
say "Complaints found, creating report ..."

# Collate logs
# The "|| true" is because this is all best-effort: we don't want to
# fail to collect info because of "set -e".
sudo cat /var/log/audit/audit.log > "${TMPDIR}/audit.log" || true
sudo ps aux > "${TMPDIR}/ps_aux.log" || true
sudo journalctl -b > "${TMPDIR}/journalctl.log" || true
uname -a > "${TMPDIR}/uname.log" || true
cp --dereference \
    /etc/image_version \
    /etc/os-release \
    "${TMPDIR}" || true
dpkg-query -W > "${TMPDIR}/dpkg-query.log" || true
cp -a /etc/apparmor.d "${TMPDIR}/apparmor.d" \
    > "${TMPDIR}/cp-apparmor-d.log" 2>&1 || true

tar -C "${AA_TMPDIR}" -cvjf ~/"${AA_DUMP}.tar.bz2" "${AA_DUMP}"
say "Report created as $HOME/${AA_DUMP}.tar.bz2"
