#!/bin/sh

set -e

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

set -x

BACKUP_FILES=""
PROCESSES_TO_KILL=""

cleanup () {
    if test -n "$BACKUP_FILES"; then
        for f in $BACKUP_FILES; do
            sudo mv $f.bak $f
        done
    fi

    if test -n "$PROCESSES_TO_KILL"; then
        for p in $PROCESSES_TO_KILL; do
            sudo pkill -9 $p
        done
    fi
}

die () {
    cleanup
    echo "telepathy-ring-automated: FAILED"

    exit 1
}

backup_file () {
    if [ ! -f $1.bak ]; then
	sudo cp $1 $1.bak || die
    fi
    BACKUP_FILES="$BACKUP_FILES $1"
}

setup () {
    phonesim_host=`host phonesim | grep address | cut -d' ' -f4 | awk '{ print $1}'`
    if test -z "$phonesim_host"; then
        ip="127.0.0.1"
        port="12345"
    else
        ip=$phonesim_host
        port="9742"
    fi

    # Setup ofono and ofono-phonesim
    sudo systemctl stop ofono || die

    backup_file /etc/ofono/phonesim.conf
    echo "[phonesim]\nAddress=$ip\nPort=$port" | sudo tee /etc/ofono/phonesim.conf > /dev/null

    sudo systemctl start ofono || die

    # Start ofono-phonesim
    if test -z "$phonesim_host"; then
        sudo ofono-phonesim -p 12345 /usr/share/phonesim/default.xml &
        PROCESSES_TO_KILL="$PROCESSES_TO_KILL ofono-phonesim"
    fi

    modem=`/usr/share/ofono/tests/list-modems | grep phonesim | cut -d' ' -f2`
    if test -z "$modem"; then
        echo "No modem available"
        die
    fi

    # Power on the modem
    sleep 3
    /usr/share/ofono/tests/enable-modem $modem || die
    /usr/share/ofono/tests/online-modem $modem || die
    sleep 3
}

setup
phoenix-test-call --testcontact=0612301 ring tel password=string:12345 || die
phoenix-test-text --testcontact=0612301 ring tel password=string:12345 || die
cleanup

echo "telepathy-ring-automated: PASSED"
