<xsl:stylesheet version="1.1"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exslt="http://exslt.org/common"
  extension-element-prefixes="exslt">

<!--
     This transformation determines what percentage of the total samples
     each task appeared in.

     Output: the percentage, number of samples and name of each task
-->

<xsl:output method="text" />

<xsl:variable name="size">
  <!-- the total number of samples in this entire run -->
  <xsl:for-each select="/profile/size">
    <!-- select the given value -->
    <xsl:value-of select="."/>
  </xsl:for-each>
</xsl:variable>

<xsl:template match="@*|node()">
  <xsl:for-each select="/profile/objects/object">
    <xsl:if test="starts-with(name, '&quot;[')">
      <xsl:value-of select="format-number(total div number($size) * 100, '#.##')"/>
      <xsl:text>% </xsl:text>
      <xsl:value-of select="total"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="name"/>
      <xsl:text>&#xa;</xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>
</xsl:stylesheet>
