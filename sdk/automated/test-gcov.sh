#!/bin/sh

# This builds a program with gcov support, runs it multiple ways, and ensures
# that coverage meets expectations
TEST_DIR=$(dirname $0)
TEST_PROG=$(mktemp)
TEST_SRC_NAME=multipath
TEST_PROG_ARGS=""
CFLAGS="-fprofile-arcs -ftest-coverage"
GCOV_OUT_FILES="$TEST_PROG.c.gcov $TEST_PROG.gcda $TEST_PROG.gcno"
MIN_PORTION="1.0"

die() {
        echo $1
        exit 0
}

#
# Clean-up from previous runs
#
rm -f $GCOV_OUT_FILES

#
# Build program with gcov support
#
cc $CFLAGS -o $TEST_PROG $TEST_DIR/$TEST_SRC_NAME.c

#
# Main test
#

# run the program multiple ways to ensure adequate coverage
$TEST_PROG >/dev/null 2>&1
$TEST_PROG -2 >/dev/null 2>&1
$TEST_PROG --invalid-option >/dev/null 2>&1

test_portion="$(gcov -o $(pwd) $TEST_DIR/$TEST_SRC_NAME.c | \
        grep "Lines executed" | \
        sed 's/.*:\([0-9.]*\)%.*/\1/')"

# Convert each to a percentage and drop any fractional part (so we can compare
# with [)
TEST_PCT=$(echo $test_portion | awk '{print int($1)}')
MIN_PCT=$(echo $MIN_PORTION 100 | awk '{print int($1*$2)}')

STATUS_STR="FAILED"
if [ $TEST_PCT -ge $MIN_PCT ]; then
        STATUS_STR="PASSED"
fi

# Clean temporary program
rm -f $TEST_PROG

echo "sdk-code-analysis-tools-gcov-smoke-test: $STATUS_STR"
echo "Program ran in $TEST_PCT% of samples (>= $MIN_PCT% required)"
