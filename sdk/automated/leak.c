/*
 * Leak memory (to be evaluated by valgrind)
 */

#include <stdlib.h>
#include <stdio.h>

static void
func_leak()
{
  int *int_ptr = malloc(sizeof (int));

  /* intentionally leak int_ptr's memory */
}

int
main(int argc, char* argv[])
{
  if(argc >= 2 && argv[1] != NULL) {
    if(strcmp (argv[1], "--leak") == 0) {
      func_leak();
    } else {
      fprintf(stderr, "A program which does nothing, or, optionally, "
          "leaks memory\n\n"
          "    usage: %s [--leak]\n", argv[0]);
    }
  }

  return 0;
}
