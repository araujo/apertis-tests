#!/bin/sh

# This builds a program with gprof support and checks that its relative time in
# each of its functions is as expected (within some margin of error)

TEST_DIR=$(dirname $0)
TEST_PROG=$(mktemp)
TEST_SRC_NAME=fib
TEST_PROG_MAJOR_FUNC=fibonacci
# calculate the Fibonacci sequence to this position. Runtime increases
# exponentially by this number
TEST_PROG_ARGS="45"
GPROF_OUT_FILE="gmon.out"
# the instrumentation added by gprof means that, even though
# TEST_PROG_MAJOR_FUNC should take the vast majority of the runtime, it won't
# take anywhere near 100% of it
MIN_PORTION="0.85"

die() {
        echo "sdk-performance-tools-gprof-smoke-test: FAILED"
        echo "ERROR: $1"
        exit 0
}

#
# Build program with gprof support
#
cc -o $TEST_PROG $TEST_DIR/$TEST_SRC_NAME.c -g -pg

#
# Main test
#
$TEST_PROG $TEST_PROG_ARGS > /dev/null >&1
test_func_portion=$(gprof -b --no-graph $TEST_PROG | \
                grep $TEST_PROG_MAJOR_FUNC | \
                sed "s/[^[:digit:]]*\([0-9.]*\).*$TEST_PROG_MAJOR_FUNC/\1/")

# Convert each to a percentage and drop any fractional part (so we can compare
# with [)
TEST_PCT=$(echo $test_func_portion | awk '{print int($1)}')
MIN_PCT=$(echo $MIN_PORTION 100 | awk '{print int($1*$2)}')

#
# Clean-up test run
#
rm -f $TEST_PROG
rm -f $GPROF_OUT_FILE

STATUS_STR="FAILED"
if [ $TEST_PCT -ge $MIN_PCT ]; then
        STATUS_STR="PASSED"
fi

echo "sdk-performance-tools-gprof-smoke-test: $STATUS_STR"
echo "Program ran in $TEST_PCT% of samples (>= $MIN_PCT% required)"
