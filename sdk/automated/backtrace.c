/*
 * Build up a simple stack trace
 */

#include <stdlib.h>
#include <stdio.h>

#define PRINT_FUNC_NAME printf("In %s()\n", __func__);

static void
func_break()
{
  PRINT_FUNC_NAME
}

/* if we break on func_break(), this shouldn't be in the backtrace */
static void
func_return_before_bt()
{
  PRINT_FUNC_NAME
}

static void
func_1()
{
  PRINT_FUNC_NAME

  func_return_before_bt();
  func_break();
}

int
main(int argc, char* argv[])
{
  func_1();

  return 0;
}
