<xsl:stylesheet version="1.1"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exslt="http://exslt.org/common"
  extension-element-prefixes="exslt">

<!--
     This transformation manipulates the sysprof output to determine the portion
     of CPU time spent on our CPU-intense process. Our final check will be
     whether this portion is significant enough to accept that sysprof
     identified our process as burning up a lot of cycles.

     More-exhaustive tests could potentially be done where we check the relative
     processing time for each function within our test process. But that seems a
     little too fragile and excessive at this point.

     Output: a decimal number in range [0, 1] representing the portion of CPU
             time spent on our test program while sysprof ran.
-->

<xsl:output method="text" />
<!-- XXX: keep this sync'd with the test script's TEST_PROG -->
<xsl:variable name="procName">fib.pl</xsl:variable>

<xsl:variable name="size">
  <!-- the total number of samples in this entire run -->
  <xsl:for-each select="/profile/size">
    <!-- select the given value -->
    <xsl:value-of select="."/>
  </xsl:for-each>
</xsl:variable>

<xsl:variable name="procTotal">
  <xsl:for-each select="/profile/objects/object">
    <!-- match the process by name. Note this only works because the process
         renames itself "[$0]" (cutting off any leading path in its name).
         Additionally, sysprof wraps every sample object name with "" (hence
         &quot;) -->
    <!-- (we can't use a regex because we can't use XSLT 2.0) -->
    <xsl:if test="starts-with(name, concat('&quot;[', $procName, ']&quot;'))">
      <xsl:value-of select="total"/>
    </xsl:if>
  </xsl:for-each>
</xsl:variable>

<xsl:template match="@*|node()">
  <xsl:value-of select="number($procTotal) div number($size)"/>
</xsl:template>
</xsl:stylesheet>
