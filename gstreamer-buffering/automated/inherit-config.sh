# This file should be symlinked inside your test directory,
# and sourced from config.sh
# It will be modified and copied to the install directory by make

. "${TESTDIR}/../../common/common.sh"

## These variables are properties of the image itself
LIBEXECDIR="${LIBEXECDIR:-/usr/lib}"

## These variables get modified by `make` during install
# Directory where scripts and other non-binary data is installed
TESTDATADIR="${TESTDIR}"
# Binary executable install directory
TESTLIBDIR="${TESTDIR}"
# Directory where test data/resources are installed
RESOURCE_DIR="$(_realpath ${TESTDIR}/../../resources)"

## These are derived from the above
MEDIA_RESOURCE_DIR="${RESOURCE_DIR}/media"
