TCMMD="${TCMMD:-/usr/bin/tcmmd}"
TCDEMO="${TCDEMO:-/usr/bin/tcdemo}"

WGET_TEST_DURATION=6

TCMMD_LOG=/tmp/tcmmd.$$.log
TCDEMO_LOG=/tmp/tcdemo.$$.log

TCMMD_PID=""

# List of processes to kill on exit
ROOT_PROC_KILL_LIST=()
PROC_KILL_LIST=()

# Kill all pids in the list. It's okay if the list has stale pids.
# Theoretically, OSes recycle pids, but that's over a long period of time.
# We're unlikely to have collisions in the (relatively) short span of this test.
_kill_procs() {
    local proc
    for proc in "${ROOT_PROC_KILL_LIST[@]}"; do
        if ps -p $proc >/dev/null; then
            # Let sudo a chance to kill its child
            sudo kill -SIGINT $proc || true
            sleep 0.2
            sudo kill -SIGKILL $proc || true
        fi
    done
    for proc in "${PROC_KILL_LIST[@]}"; do
        if ps -p $proc >/dev/null; then
            # Only killing wget here. No need for sudo.
            kill $proc || true
        fi
    done
}

_cleanup() {
  _kill_procs
}

title() {
  TITLE="$1"

  underline=`tput smul`
  nounderline=`tput rmul`
  bold=`tput bold`
  normal=`tput sgr0`

  say "${bold}${underline}${TITLE}${nounderline}${normal}"
}

#########
# Setup #
#########

trap "setup_failure" ERR

# better check that at the beginning instead of asking questions and fail after
check_not_root
ensure_dbus_session

if [ ! -x $TCMMD ] ; then
  cry "tcmmd is not installed"
  exit 1
fi

if [ ! -x $TCDEMO ] ; then
  cry "tcdemo is not installed"
  exit 1
fi

if ! which wget &> /dev/null ; then
  cry "wget is not installed"
  exit 1
fi

if pidof tcmmd &> /dev/null ; then
  cry "tcmmd is already running"
  exit 1
fi

say "To avoid the questions, you could use the environment variables NET_INTERFACE, URL, SERVER_IP"

if [ -z "$NET_INTERFACE" ] ; then
  whine "Please enter the network interface (e.g. eth0 or wlan0):"
  read NET_INTERFACE
fi
say "Network interface is $NET_INTERFACE"

IP="$(ifconfig $NET_INTERFACE | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}')"
if [ -z "$IP" ] ; then
  cry "Cannot find my IP"
  exit 1
fi
say "My IP is $IP"

if [ -z "$URL" ] ; then
  whine "Please enter the URL:"
  read URL
fi
say "URL is $URL"

if [ -z "$SERVER_IP" ] ; then
  whine "Please enter the server IP:"
  read SERVER_IP
fi
say "Server IP is $SERVER_IP"

