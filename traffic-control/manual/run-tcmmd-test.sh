#!/bin/bash
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - &>/dev/null)
. "${TESTDIR}/config.sh"

# Initialization and setup shared between the test scripts
. "${TESTDIR}/init-setup.sh"
setup_success

###########
# Execute #
###########

start_rule() {
  TITLE="$1"

  BIND_IPSRC=$2
  BIND_TCPSPORT=$3

  ENFORCED_IPSRC=$4
  ENFORCED_IPDST=$5
  ENFORCED_TCPSPORT=$6
  ENFORCED_TCPDPORT=$7
  ENFORCED_BACKGROUND_RATE=$8
  ENFORCED_PRIORITY_RATE=$9

  EXPECTED_BACKGROUND_RATE=${10}
  EXPECTED_PRIORITY_RATE=${11}

  title "Test: $TITLE"

  ${TESTDIR}/client.py \
    --ipsrc "$ENFORCED_IPSRC" --ipdst "$ENFORCED_IPDST" \
    --tcpsport "$ENFORCED_TCPSPORT" --tcpdport "$ENFORCED_TCPDPORT" \
    --backgroundrate "$ENFORCED_BACKGROUND_RATE" \
    --priorityrate "$ENFORCED_PRIORITY_RATE" &
  CLIENT_PID=$!

  say "Please look at the download rate."
  sleep 0.5
  wget -T 5 -O /dev/null $URL &
  WGET_PID=$!
  PROC_KILL_LIST+=($WGET_PID)

  sleep $WGET_TEST_DURATION
  kill $WGET_PID || true
  sleep 0.2
  whine "Was the limit of $EXPECTED_BACKGROUND_RATE correctly enforced [yes/no]?"
  read v
  if [ "$v" != "yes" ] ; then
    kill $CLIENT_PID || true
    return 1
  fi

  say "Please look at the download rate."
  sleep 0.5
  say wget -T 5 --bind-address="$BIND_IPSRC" --bind-port="$BIND_TCPSPORT" -O /dev/null $URL
  wget -T 5 --bind-address="$BIND_IPSRC" --bind-port="$BIND_TCPSPORT" -O /dev/null $URL &
  WGET_PID=$!
  PROC_KILL_LIST+=($WGET_PID)

  sleep $WGET_TEST_DURATION
  kill $WGET_PID || true
  sleep 0.2
  whine "Was the limit of $EXPECTED_PRIORITY_RATE correctly enforced [yes/no]?"
  read v
  if [ "$v" != "yes" ] ; then
    kill $CLIENT_PID || true
    return 1
  fi

  kill $CLIENT_PID
}

check_decent_speed() {
  TITLE="$1"

  title "Test: $TITLE"

  say "Please look at the download rate."
  wget -T 5 -O /dev/null $URL &
  WGET_PID=$!
  PROC_KILL_LIST+=($WGET_PID)

  sleep $WGET_TEST_DURATION
  kill -9 $WGET_PID || true
  wait $WGET_PID || true
  sleep 0.2
  whine "Was it a decent speed (1MB/s or more) [yes/no]?"
  read v
  if [ "$v" != "yes" ] ; then
    sudo kill $TCMMD_PID || true
    return 1
  fi

  return 0
}

check_tcmmd() {
  sudo $TCMMD -i $NET_INTERFACE &>/dev/null &
  TCMMD_PID=$!
  ROOT_PROC_KILL_LIST+=($TCMMD_PID)
  sleep 0.5
  if ! sudo kill -s 0 $TCMMD_PID &> /dev/null ; then
    return 1
  fi
  sleep 0.5

  # there is no point to run the main tests if we don't have a good connectivity
  check_decent_speed "check decent speed before the main tests"
  if [ $? != 0 ] ; then
    sudo kill $TCMMD_PID || true
    return 1
  fi

  say "Ok, let's start the main tests."

  TCPSPORT=$(( 12000 + $RANDOM % 999 ))

  titles[0]="TC rule checking the tuple (ipsrc, ipdst, tcpsport, tcpdport)"
  tests_func[0]=start_rule
  tests_params[0]="$IP $TCPSPORT $IP $SERVER_IP $TCPSPORT 80 10000 80000 10KB/s 80KB/s"

  titles[1]="TC rule checking only tcpsport"
  tests_func[1]=start_rule
  tests_params[1]="$IP $TCPSPORT 0.0.0.0 0 $TCPSPORT 0 10000 80000 10KB/s 80KB/s"

  titles[2]="TC rule with a wrong ipsrc"
  tests_func[2]=start_rule
  tests_params[2]="$IP $TCPSPORT 8.8.8.8 $SERVER_IP $TCPSPORT 80 10000 80000 10KB/s 10KB/s"

  titles[3]="TC rule with a wrong ipdst"
  tests_func[3]=start_rule
  tests_params[3]="$IP $TCPSPORT $IP 8.8.8.8 $TCPSPORT 80 10000 80000 10KB/s 10KB/s"

  titles[4]="TC rule with a wrong tcpsport"
  tests_func[4]=start_rule
  tests_params[4]="$IP $TCPSPORT $IP $SERVER_IP $(( $TCPSPORT + 1 )) 80 10000 80000 10KB/s 10KB/s"

  titles[5]="TC rule with a wrong tcpdport"
  tests_func[5]=start_rule
  tests_params[5]="$IP $TCPSPORT $IP $SERVER_IP $TCPSPORT 81 10000 80000 10KB/s 10KB/s"

  titles[6]="check if decent speed is restored after the main tests"
  tests_func[6]=check_decent_speed
  tests_params[6]=

  for i in $(seq 0 $((${#titles[*]}-1))) ; do
    ${tests_func[$i]} "${titles[$i]}" ${tests_params[$i]}
    tests_result[$i]=$?
  done

  title "Test results"
  for i in $(seq 0 $((${#titles[*]}-1))) ; do
    echo -n "${titles[$i]}: "
    if [ ${tests_result[$i]} == 0 ] ; then
      echo_green PASS
    else
      echo_red FAIL
    fi
    echo
  done
  echo

  sudo kill $TCMMD_PID || true

  check_decent_speed "check if connectivity is not broken after killing tcmmd"
  if [ $? != 0 ] ; then
    sudo kill $TCMMD_PID || true
    return 1
  fi

  return 0
}

trap "_cleanup; test_failure" ERR
trap "_cleanup" EXIT

check_tcmmd

