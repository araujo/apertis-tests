#! /usr/bin/python3
# -*- coding: utf-8 -*-
#
# BlueZ - Bluetooth protocol stack for Linux
#
# Copyright © 2012, 2015 Collabora Ltd.
#
# SPDX-License-Identifier: GPL-2.0+
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from gi.repository import GObject

import argparse
import sys
import dbus
import dbus.service
import dbus.mainloop.glib
import os

# import from toplevel directory
sys.path.insert(0, os.path.join(os.path.dirname(__file__), os.pardir))
from apertis_tests_lib.bluez import AskAgent
from apertis_tests_lib.bluez import adapters_make_pairable
from apertis_tests_lib.bluez import build_device_path
from apertis_tests_lib.bluez import get_hci
from apertis_tests_lib.bluez import scan_for_device

if __name__ == '__main__':
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    mainloop = GObject.MainLoop()

    parser = argparse.ArgumentParser()
    parser.add_argument("-y", "--yes", help="Accept all connections",
        action="store_true")
    parser.add_argument("-d", "--device", help="HCI device to use")
    parser.add_argument("-c", "--capability", default="KeyboardDisplay")
    parser.add_argument("address", help="Address to pair", nargs='?')
    args = parser.parse_args()

    def pair_reply():
        print("New device (%s)" % (args.address))
        mainloop.quit()

    def pair_error(error):
        print("Creating device failed: %s" % (error))
        mainloop.quit()

    bus = dbus.SystemBus()

    connman_bt_tech = dbus.Interface(
        bus.get_object("net.connman", "/net/connman/technology/bluetooth"),
        "net.connman.Technology")

    def property_changed(name, value):
        if name == "Powered" and value:
            print("Bluetooth powered on")
        mainloop.quit()

    # Enable the connman bluetooth technology so that connman removes its
    # rfkill on the bluetooth adaptors
    match = connman_bt_tech.connect_to_signal("PropertyChanged",
            property_changed)
    try:
        connman_bt_tech.SetProperty("Powered", True)
        mainloop.run()
    except dbus.DBusException as e:
        if e.get_dbus_name() != "net.connman.Error.AlreadyEnabled":
            raise(e)
    finally:
        match.remove()

    manager = dbus.Interface(bus.get_object("org.bluez", "/"),
                             "org.freedesktop.DBus.ObjectManager")
    agent_manager_iface = dbus.Interface(bus.get_object('org.bluez',
                                                        '/org/bluez'),
                                         'org.bluez.AgentManager1')

    objs = manager.GetManagedObjects()
    adapters = {p: i for p, i in objs.items()
                if 'org.bluez.Adapter1' in i.keys()}

    if args.device:
        path = '/org/bluez/' + args.device
    else:
        path = list(adapters.keys())[0]

    adapter_obj = bus.get_object("org.bluez", path)
    adapter = dbus.Interface(adapter_obj,
                             "org.bluez.Adapter1")

    agent = AskAgent(bus, '/test/agent', mainloop,
        authorize_everything = args.yes)

    if args.address:
        agent.set_exit_on_release(False)

        def pair(device):
            device.Pair(dbus_interface='org.bluez.Device1',
                    reply_handler=pair_reply,
                    error_handler=pair_error,
                    timeout=60000)

        scan_for_device(bus, adapter_obj, args.address, pair)
    else:
        adapters_make_pairable([adapter_obj])
        agent_manager_iface.RegisterAgent('/test/agent', args.capability)
        agent_manager_iface.RequestDefaultAgent('/test/agent')
        print("Agent registered")

    mainloop.run()
