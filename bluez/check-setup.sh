#!/bin/bash

ps uax | grep -v grep | grep -q bluetoothd
if [ $? -eq 1 ]; then echo "bluetoothd is not running" && exit 1; fi

ps uax | grep -v grep | grep -q ofonod
if [ $? -eq 1 ]; then echo "ofonod is not running" && exit 1; fi

ps uax | grep -v grep | grep -q connmand
if [ $? -eq 1 ]; then echo "connmand is not running" && exit 1; fi

hciconfig -a
if [ ! $? -eq 0 ]; then echo "hciconfig -a failed" && exit 1; fi

sudo hciconfig hci0 reset
if [ ! $? -eq 0 ]; then echo "hciconfig hci0 reset failed" && exit 1; fi

sudo hciconfig hci0 down
if [ ! $? -eq 0 ]; then echo "hciconfig hci0 down failed" && exit 1; fi

sudo hciconfig hci0 up
if [ ! $? -eq 0 ]; then echo "hciconfig hci0 up failed" && exit 1; fi

sudo hciconfig hci0 piscan
if [ ! $? -eq 0 ]; then echo "hciconfig hci0 piscan failed" && exit 1; fi

sudo hciconfig hci0 pscan
if [ ! $? -eq 0 ]; then echo "hciconfig hci0 pscan failed" && exit 1; fi

echo "Put some devices around in discoverable mode and press ENTER"

read

hcitool scan

echo ""
echo "Check if the devices you enabled are in the list above"
