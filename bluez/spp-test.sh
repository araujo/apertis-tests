#!/bin/bash

SCRIPT_PATH=`dirname $0`
$SCRIPT_PATH/pair-two

if [ $? -eq 1 ] ; then
	echo "Pairing failed"
	exit 1
fi

echo -n "Test 1: No encryption ... "
rctest -i hci0 -a hci1 -B $SCRIPT_PATH/rc.in -O /tmp/rc.out -b 20000 -P 27 2> /dev/null
sleep 1
ERR=$(diff -q $SCRIPT_PATH/rc.in /tmp/rc.out 2>&1)
if [ $? -ne 0 ] ; then
    echo "FAILED"
    echo $ERR 1>&2
else
    echo "PASSED"
fi
rm -f /tmp/rc.out

echo -n "Test 2: Encryption ... "
rctest -i hci0 -a hci1 -B $SCRIPT_PATH/rc.in -O /tmp/rc.out -b 20000 -P 28 -E 2> /dev/null
sleep 1
ERR=$(diff -q $SCRIPT_PATH/rc.in /tmp/rc.out 2>&1)
if [ $? -ne 0 ] ; then
    echo "FAILED"
    echo $ERR 1>&2
else
    echo "PASSED"
fi
rm -f /tmp/rc.out

echo -n "Test 3: Secure ... "
rctest -i hci0 -a hci1 -B $SCRIPT_PATH/rc.in -O /tmp/rc.out -b 20000 -P 29 -S 2> /dev/null
sleep 1
ERR=$(diff -q $SCRIPT_PATH/rc.in /tmp/rc.out 2>&1)
if [ $? -ne 0 ] ; then
    echo "FAILED"
    echo $ERR 1>&2
else
    echo "PASSED"
fi
rm -f /tmp/rc.out
