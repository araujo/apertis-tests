#include <stdlib.h>
#include <clutter/clutter.h>
#include "cb-button.h"

static const ClutterColor stage_color = { 0x33, 0x33, 0x55, 0xff };

void
clicked_cb (ClutterClickAction *action,
            ClutterActor       *actor,
            gpointer            user_data)
{
  g_print ("Pointer button %d clicked on actor %s\n",
  clutter_click_action_get_button (action),
  clutter_actor_get_name (actor));
}

int
main (int   argc,
      char *argv[])
{
  ClutterActor *stage;
  ClutterAction *action1;
  ClutterAction *action2;
  ClutterAction *action3;
  ClutterActor *actor1;
  ClutterActor *actor2;
  ClutterActor *actor3;

  if (clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
    return 1;

  stage = clutter_stage_new ();
  clutter_actor_set_size (stage, 800, 800);
  clutter_actor_set_background_color (stage, &stage_color);
  g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

  actor1 = cb_button_new();
  clutter_actor_set_name (actor1, "Red Button");
  cb_button_set_background_color (CB_BUTTON(actor1), CLUTTER_COLOR_Red);
  clutter_actor_set_size (actor1, 100, 100);
  clutter_actor_set_reactive (actor1, TRUE);
  clutter_actor_set_position (actor1, 150, 150);
  clutter_actor_add_child (stage, actor1);

  actor2 = cb_button_new ();
  clutter_actor_set_name (actor2, "Blue Button");
  cb_button_set_background_color (CB_BUTTON(actor2), CLUTTER_COLOR_Blue);
  clutter_actor_set_size (actor2, 200, 200);
  clutter_actor_set_position (actor2, 400, 400);
  clutter_actor_set_reactive (actor2, TRUE);
  clutter_actor_add_child (stage, actor2);

  actor3 = cb_button_new ();
  clutter_actor_set_name (actor3, "Green Button");
  cb_button_set_background_color (CB_BUTTON(actor3), CLUTTER_COLOR_Green);
  clutter_actor_set_size (actor3, 100, 100);
  clutter_actor_set_position (actor3, 200, 500);
  clutter_actor_set_reactive (actor3, TRUE);
  clutter_actor_add_child (stage, actor3);

  action1 = clutter_click_action_new ();
  clutter_actor_add_action (actor1, action1);
  g_signal_connect (action1,
                    "clicked",
                    G_CALLBACK (clicked_cb),
                    NULL);


  action2 = clutter_click_action_new ();
  clutter_actor_add_action (actor2, action2);
  g_signal_connect (action2,
                    "clicked",
                    G_CALLBACK (clicked_cb),
                    NULL);

  action3 = clutter_click_action_new ();
  clutter_actor_add_action (actor3, action3);
  g_signal_connect (action3,
                    "clicked",
                    G_CALLBACK (clicked_cb),
                    NULL);

  clutter_actor_show (stage);

  clutter_main ();

  return EXIT_SUCCESS;
}
